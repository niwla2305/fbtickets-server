from fritzbox import FritzBox
from flask import Flask, request, jsonify, render_template, session, redirect
import json
from redis import Redis
import secrets
import os

app = Flask(__name__)
app.config["SECRET_KEY"] = secrets.token_hex()

with open("config/config.json", "r") as file:
    config = json.loads(file.read())

redis = Redis(host=os.environ["REDIS_HOST"], port=os.environ["REDIS_PORT"])


def get_ticket_count_for_client(client):
    return int(redis.get(f"clients:{client}") or b'0'.decode())


@app.route('/ticket/<client>/count')
def get_ticket_count(client):
    try:
        client_config = config["clients"][client]
    except KeyError:
        return "no such client"

    token = request.args.get("token")
    if token != client_config["token"]:
        return "invalid token"

    available_tickets = get_ticket_count_for_client(client)

    return jsonify({"count": available_tickets})


@app.route('/ticket/<client>')
def get_ticket(client):
    try:
        client_config = config["clients"][client]
    except KeyError:
        return jsonify({"error": "no such client"})

    token = request.args.get("token")
    if token != client_config["token"]:
        return jsonify({"error": "invalid token"})

    available_tickets = get_ticket_count_for_client(client)
    if available_tickets == 0:
        return jsonify({"error": "no more tickets", "remaining_tickets": 0})

    box = FritzBox("fritz8400", "Ilafmi!")
    box.login()
    tickets = box.get_tickets()

    available_tickets -= 1
    redis.set(f"clients:{client}", available_tickets)

    return jsonify({"ticket": tickets[0], "remaining_tickets": available_tickets})


@app.route('/')
def index():
    return redirect("/admin/add_tickets")


@app.route('/admin/login')
def login():
    return render_template("login.html")


@app.route('/admin/login', methods=["POST"])
def login_post():
    if session.get("is_authenticated"):
        return redirect("/admin/add_tickets")
    if request.form.get("password") == config["admin_password"]:
        session["is_authenticated"] = True
        return redirect("/admin/add_tickets")
    return redirect("/admin/login")


@app.route('/admin/add_tickets')
def add_tickets_form():
    if not session.get("is_authenticated"):
        return redirect("/admin/login")
    clients = config["clients"]
    clients2 = {}
    for key, value in clients.items():
        clients2[key] = {"description": value["description"], "count": get_ticket_count_for_client(key)}
    return render_template("add_tickets.html", clients=clients2)


@app.route('/admin/add_tickets', methods=["POST"])
def add_tickets():
    if not session.get("is_authenticated"):
        return redirect("/admin/login")
    try:
        count = int(request.form["count"])
        client = request.form["client"]
    except KeyError:
        return "missing fields"
    except ValueError:
        return "wrong type for count"
    current_count = get_ticket_count_for_client(client)
    redis.set(f"clients:{client}", current_count + count)
    return render_template("success.html", count=current_count + count)
