import requests

import login
from bs4 import BeautifulSoup


class FritzBox:
    def __init__(self, username, password, url="http://192.168.178.1"):
        self.url = url
        self.username = username
        self.password = password
        self.sid = None

    def login(self):
        self.sid = login.get_sid(self.url, self.username, self.password)

    def get_tickets(self):
        response = requests.post(self.url + "/data.lua",
                                 data={"xhr": "1", "sid": self.sid, "lang": "de", "page": "kidPro"})
        soup = BeautifulSoup(response.text, features="lxml")
        tickets = soup.find(id="uiTickets")
        tickets_plain = []
        for ticket in tickets.find_all("td"):
            tickets_plain.append(ticket.text)

        return tickets_plain
